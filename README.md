# Microblog v0.8

## Updates

### v0.8
* Added support for paginating posts

### v0.7 
* Added support for followers

### v0.6
* File based error logging

### v0.5
* Added Profile page
* Added Edit Profile page
* Added Support for Gravatar avatars

### v0.4
* Added Logic to handle login
* Added logic to handle logout
* Added Registration form and logic

### v0.3
* Added Login Form

### v0.2
* Added Sub-templates
* Add database support(sqlite3)
