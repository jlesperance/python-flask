from app import app, db
from app.models import User, Post, Goal, Form, GoalItems, Technique, UserGoal

@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User, 'Post': Post, 'Goal': Goal, 'Form': Form, 'GoalItems': GoalItems, 'Technique': Technique, 'UserGoal': UserGoal}
